program mm_reuse

integer, parameter :: m = 2048 ! matrix size
integer :: i,j,k  ! Matrix, indices
real, dimension(1:m,1:m) :: A, B, C
real :: sum

! Initialize the arrays 

do j = 1,m 
   do i = 1,m
      A(i,j) = 1
      B(i,j) = 1
      C(i,j) = 0
   end do
end do

do j = 1,m
   do i = 1,m
      sum = 0
      do k = 1,m
         sum  = sum + A(i,k)*B(k,j)
      end do
      C(i,j) = sum
   end do
end do

! do j = 1,m
!    do i = 1,m
!       print*, "C(",i,",",j,") = ", C(i,j)
!    end do
! end do

end program
