program mm_noreuse

integer, parameter :: m = 2049 ! matrix size
integer :: i,j,k  ! Matrix, indices
real, dimension(1:m,1:m) :: A, B, C

! Initialize the arrays, looping j first since fortran is column major 

do j = 1,m 
   do i = 1,m
      A(i,j) = 1
      B(i,j) = 1
      C(i,j) = 0
   end do
end do

do j = 1,m
   do i = 1,m
      do k = 1,m
         C(i,j) = C(i,j) + A(i,k)*B(k,j)
      end do
   end do
end do

! do j = 1,m
!    do i = 1,m
!       print*, "C(",i,",",j,") = ", C(i,j)
!    end do
! end do

end program
