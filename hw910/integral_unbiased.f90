program integral_unbiased
            
  REAL :: x1, f, integral, domain, sum
  INTEGER :: counter, i

  call random_seed()
  accept = 0
  counter = 10
  domain = 10
  sum = 0.0
 
  do i = 1,100000
     call random_number(x1)
     x1 = 10*x1 - 5
     f = exp(-x1**2/2)*(cos(x1)+5)
     sum = sum + f
     if(i == counter) then
        integral = sum/i*domain
        print*, i, integral
        counter = counter*10
     end if
  end do
  
end program
