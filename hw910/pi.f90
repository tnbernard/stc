program pi
            
  REAL :: x(100000), y(100000)
  REAL :: x1, y1, pi_value
  INTEGER :: accept, counter

  call random_seed()
  pi_value = 3.141592653589793238
  accept = 0
  counter = 10
  
  !open (unit = 1, file = "pi.dat", action = "write")
  do i = 1,100000
     call random_number(x1)
     x1 = 2*x1 - 1
     call random_number(y1)
     y1 = 2*y1 - 1
     r = sqrt(x1**2 + y1**2)
     if (r < 1) then 
        x(i) = x1
        y(i) = y1
        accept = accept + 1
        !write(1,*) x(i), y(i)
     end if
     if(i == counter) then
        pi_value = 4*float(accept)/i
        print*, i, pi_value
        counter = counter*10
     end if
  end do
  
end program
