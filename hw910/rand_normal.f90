program rand_norm

  REAL :: x(100000)

  open (unit = 1, file = "norm.dat", action = "write")
  do i = 1,100000
     call norm_dist(x(i))      
     write(1,*) x(i)   
  end do

end program rand_norm

subroutine norm_dist(x)
  REAL, INTENT(out) :: x
  REAL :: u1, y1, u2, y2, u3, test
  INTEGER :: counter, seed
  
  seed = 12345
  call random_seed(seed)
  do
     call random_number(u1)
     y1 = -log(u1)
     call random_number(u2)
     y2 = -log(u2)
     test  = (y1-1)**2/2
     if (y2 >= test) then 
        x = y1
        call random_number(u3)
        if ( u3 <= 0.5 ) then
           x = abs(x)
        else 
           x = -abs(x)
        end if
        exit
     end if
  end do

end subroutine norm_dist

