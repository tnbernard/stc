program rand_exp

  REAL :: x(100000)
  REAL :: lambda, r
  
  CALL random_seed()
  
  lambda = 1.0/3.0

  open (unit = 1, file = "exp.dat", action = "write")
  do i = 1,100000
     do
        call random_number(r)
        x(i)  = - 1/lambda*log(r/lambda)
        if (x(i) > 0) then 
           exit
        end if
     end do
     write(1,*) x(i)
  end do

end program
