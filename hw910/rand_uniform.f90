program rand_uniform

  REAL :: r(100000)
  CALL random_seed()
  CALL RANDOM_NUMBER(r)
  
  r = r*10 - 5
  open (unit = 1, file = "uniform.dat", action = "write")
  do i = 1,100000
     write(1,*) r(i)
  end do

end program
