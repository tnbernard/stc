! SDS 394 HW 7
! Name: Tess Bernard
! Stampede ID: tnb576
! Bitbucket ID: tnbernard

! This is a program to test the Gauss-Seidel iterative method

program gauss_seidel

  implicit none
  real*8 :: s, l2, l2_old, epsilon, error
  real*8, dimension(3,3) :: a
  real*8, dimension(3) :: b, theta_k, theta_old
  integer :: i, j, k, k_max

  epsilon = 1e-8  
  k_max = 10000
  a = reshape((/ 1, 2, 3, 1, 3, 2, 2, 5, 6 /), shape(a)) ! This is the matrix from the assignment.
  b = (/12, 18, 36 /) !This is the solution from the assigment.
  !a = reshape((/ 2,4,9,3,2,6,2,3,7 /), shape(a)) ! Another unimodular, Det=1, matrix
  !a = reshape((/ -3,-4,5,3,7,7,-6,-8,-9 /), shape(a)) ! Should converge for Jacobi but not GS
  !a = reshape((/ 3,7,-1,0,4,1,4,2,2 /), shape(a))  ! Should converge for GS but not Jacobi
  
  ! The following system converges for both
  !a = reshape((/ 4,-2,-1,-1,6,1,-1,1,7 /), shape(a))
  !b = (/ 3, 9, 6 /)

  ! Print a
  10 format(15E20.10)

  print*, "A = "
  do, i=1,3
     write(*,10) ( a(i,j), j=1,3 )
  end do

  ! Print b
  print*, "b = "
  do i=1,3
     print*, b(i)
  end do

  ! Choose theta_0 from lower triangular matrix
  print*, "theta_0 = "
  do i = 1,3
     s = b(i)/a(i,i)
     do j = 1,3
        if (i > j) then
           s = s - a(i,j)*theta_old(j)/a(i,i)
        end if
     end do
     theta_old(i) = s
     print*, theta_old(i)
  end do

  k = 0
  do 
     do i = 1,3
        s = 0
        do j = 1,3
           if (j > i) then 
              s = s + a(i,j)*theta_old(j)
           else if (j < i) then 
              s = s + a(i,j)*theta_k(j)
           end if
        end do
        theta_k(i) = (b(i) - s)/a(i,i)
     end do

     ! Check for convergence by calculating l2-norm of difference of theta_k anda theta_old
     l2 = 0
     l2_old =0
     do i = 1,3
        l2 = l2 + (theta_k(i) - theta_old(i))**2
        l2_old = l2_old + theta_old(i)**2
     end do
     l2 = sqrt(l2)
     l2_old = sqrt(l2_old)
     error = l2/l2_old
     !print*, "Error = ", error
     if (error <= epsilon) then
        print*, "Convergence successful to within", epsilon,". Solved in ", k, " iterations."
        print*, "x = "
        do i=1,3
           print*, theta_k(i)
        end do
        exit
     else if (k > k_max) then
        print*, "Convergence unsuccessful in",k_max,"iterations."
        exit
     else
        k = k + 1
        do i = 1,3
           theta_old(i) = theta_k(i)
        end do
     end if
  end do

end program
