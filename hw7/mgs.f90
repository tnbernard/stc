! SDS 394 HW 7
! Name: Tess Bernard
! Stampede ID: tnb576
! Bitbucket ID: tnbernard

! This is a program to test the Modified Gram-Schmidt iterative method

program mgs

  implicit none
  real*8 :: s, l2, l2_old, error, alpha, beta
  real*8, dimension(3,3) :: a, v, r, q, c, qt
  real*8, dimension(3) :: b, b_old, x
  integer :: i, j, k, n

  n = 3
  alpha = 1e0
  beta = 0e0

  print*, "Iterative solution using Modified Gram-Schmidt method"
  a = reshape((/ 1, 2, 3, 1, 3, 2, 2, 5, 6 /), shape(a))
  b = (/12, 18, 36 /)
  
  ! Try another system
  ! a = reshape((/ 4,-2,-1,-1,6,1,-1,1,7 /), shape(a))
  ! b = (/ 3, 9, 6 /)

  ! Print a
  10 format(15E20.10)

  print*, "A = "
  do, i=1,3
     write(*,10) ( a(i,j), j=1,3 )
  end do

  ! Print b
  print*, "b = "
  do i=1,3
     print*, b(i)
  end do

  ! Set v vectors equal to columns of a
  do i = 1,3
     do j = 1,3
        v(i,j) = a(i,j)
     end do
  end do

  ! print*, "V = "
  ! do, i=1,3
  !    write(*,10) ( v(i,j), j=1,3 )
  ! end do

  do i = 1,3
     s = 0
     do j = 1,3
        !print*, "v(",j,",",i,") = ", v(j,i)
        s = s + v(j,i)*v(j,i)
     end do
     !print*, "before squaring", s 
     s = sqrt(s)
     r(i,i) = s
     !r(i,i) = sqrt(v(1,i)**2 + v(2,i)**2 + v(3,i)**2)
     !print*, r(i,i)
     do j = 1,3
        q(j,i) = v(j,i)/r(i,i)
     end do
     do j = i+1,3
        s = 0
        do k = 1,3
           s = s + q(k,i)*v(k,j)
           !s = s + a(k,j)*q(k,i)
        end do
        r(i,j) = s
        do k = 1,3
           v(k,j) = v(k,j) - r(i,j)*q(k,i)
        end do
     end do
  end do

  print*, "Q = "
  do, i=1,3
     write(*,10) ( q(i,j), j=1,3 )
  end do

  print*, "R = "
  do, i=1,3
     write(*,10) ( r(i,j), j=1,3)
  end do

! Check to see that Q*R = A

  ! call dgemm('n', 'n', n, n, n, alpha, q, n, r, n, beta, c, n)

  ! print*, "Q*R = "
  ! do, i=1,3
  !    write(*,10) ( c(i,j), j=1,3)
  ! end do

! Compute Q^T

  do i = 1,3
     do j = 1,3
        qt(i,j) = q(j,i)
     end do
  end do

  ! call dgemm('n', 'n', n, n, n, alpha, q, n, qt, n, beta, c, n)

  ! print*, "Q*Q^T = "
  ! do, i=1,3
  !    write(*,10) ( c(i,j), j=1,3)
  ! end do

! Now compute Q^T*b for new b
  do i = 1,3
     b_old(i) = b(i)
  end do

  do i = 1,3
     s = 0
     do j = 1,3
        s = s + qt(i,j)*b_old(j)
     end do
     b(i) = s
  end do

! Now solve Rx = b_new by back substitution 
  do i = 3,1,-1
     s = b(i)/r(i,i)
     do j = 1,3
        if(j > i) then 
           s = s - x(j)*r(i,j)/r(i,i)
        end if
     end do
     x(i) = s
  end do

  print*, "x = "
  do i = 1,3
     print*,x(i)
  end do

end program
