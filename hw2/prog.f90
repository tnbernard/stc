program prog

integer :: i,j  ! loop indices 
integer, dimension(7) :: N  ! number of grid elements to integrate
integer :: M
real :: x_start, x_end
real :: h, midp_i, trap_i, simp_i, midp_err, trap_err, simp_err, xi, midp, trap, simp
real :: exact  ! variable for exact solution of definite integral
real :: start, finish

x_start = 0; x_end = 1

do i=1,7
   N(i) = 10**i
end do

! calculate exact solution 
exact = 1/8.0*(x_end**8 - x_start**8)

print*, "      M          midp_i          trap_i          simp_i          midp_err        trap_err        simp_err"

do i=1,7
  
   M = N(i)
   h = (x_end - x_start)/real(M)
   midp_i = 0; simp_i = 0; simp_i = 0
   midp_err = 0; trap_err = 0;  simp_err = 0
    
   ! midpoint sum
   call cpu_time(start)
   do j=0, M-1
      xi = x_start + h*j
      midp_i =  midp_i + midp(xi,h)
   end do
   call cpu_time(finish)
   print*, "midp time for M = ", M," is ", finish-start
   
   ! trap sum
   call cpu_time(start)
   do j=0, M
      xi = x_start + h*j
      trap_i =  trap_i + trap(xi,h)
   end do
   call cpu_time(finish)
   print*, "trap time for M = ", M," is ", finish-start 

   ! simp sum
   call cpu_time(start)
   do j=0, M/2+1
      xi = x_start + h*j*2
      simp_i =  simp_i + simp(xi,h)
   end do
   call cpu_time(finish)
   print*, "simp time for M = ", M, " is ", finish-start

   ! Multiply by appropriate factors of h
   midp_i = h*midp_i
   trap_i = h/2.0*trap_i
   simp_i = h/3.0*simp_i

   ! Calculate error
   midp_err = abs(midp_i - exact)
   trap_err = abs(trap_i - exact)
   simp_err = abs(simp_i - exact)

   print*, M, midp_i, trap_i, simp_i, midp_err, trap_err, simp_err
 
end do

end program
