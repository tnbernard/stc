! Create a function for the midpoint rule, trap rule and Simp rule
real function  midp(xi, h)
  implicit none
  real, intent(in)    :: xi, h
  midp = (xi+h*0.5)**7
end function midp

real function trap(xi, h)
  implicit none
  real, intent(in)    :: xi, h
  trap = xi**7 + (xi+h)**7
end function trap

real function simp(xi, h)
  implicit none
  real, intent(in)   :: xi, h
  simp = (xi-h)**7 + 4*xi**7 + (xi+h)**7
end function simp
