program linear_equation

  implicit none
  integer, parameter :: n = 5
  real(kind=8), dimension(n,n) :: a, a1, b, c 
  real(kind=8) :: alpha, beta
  character(LEN=1)  :: no_trans
  integer :: i, j, info, lda, ldb, nrhs
  integer, dimension(n) :: ipiv

  nrhs = n
  lda = n
  ldb = n

  alpha = 1e0
  beta = 0e0

  no_trans = 'n'

  do j = 1,n
     do i = 1,n
        a(i,j) = 1/float(i+j-1)
        !print*, "a(",i,",",j,") = ", a(i,j)
        if (i == j) then
           b(i,j) = 1
        else 
           b(i,j) = 0
        end if
     end do
  end do

  a1 = a

10 format(10E16.6)
  print*, "A = "
  do, i=1,n
    write(*,10) ( a(i,j), j=1,n )
  end do

  call dgesv(n, nrhs, a1, lda, ipiv, b, ldb, info)

  print*, "A^(-1) = "
  do, i=1,n
    write(*,10) ( b(i,j), j=1,n )
  end do
     
! Now check the multiplication 

  call dgemm(no_trans, no_trans, n, n, n, alpha, b, n, a, n, beta, c, n)

  print*, "A^(-1)*A = "
  do, i=1,n
    write(*,10) ( c(i,j), j=1,n )
  end do

  call dgemm(no_trans, no_trans, n, n, n, alpha, a, n, b, n, beta, c, n)

  print*, "A*A^(-1) = "
  do, i=1,n
    write(*,10) ( c(i,j), j=1,n )
  end do

end program
