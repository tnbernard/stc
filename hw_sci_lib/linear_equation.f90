program linear_equation

  implicit none
  integer, parameter :: n = 3
  real(kind=8), dimension(3) :: b, x
  real(kind=8), dimension(3,3) :: a
  integer :: i, j, info, lda, ldb, nrhs
  integer, dimension(n) :: ipiv

  nrhs = 1
  lda = n
  ldb = n
  
  a = reshape((/6,12,3,-2,-8,-13,2,6,3/), shape(a))
  b = (/16,26,-19/)

  ! do j = 1,n
  !    do i = 1,n
  !       print*, a(i,j)
  !    end do
  ! end do

  call dgesv(n, nrhs, a, lda, ipiv, b, ldb, info)

1 format(10f10.3)

  x = b
  do i = 1,n
     write(*,1) x(i)
  end do

end program
