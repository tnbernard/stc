!  This file contains all routines related to test functions.  

subroutine nf0(xin,fpoints,b_inv,f)
  implicit none
  real*8, intent(in) :: xin
  real*8, dimension(0:7), intent(in) :: fpoints
  real*8, dimension(0:7,0:7), intent(in) :: b_inv
  real*8, intent(out) :: f
  real*8, dimension(0:7)  :: x, basis, w
  integer :: i,n
  
  f = 0
  n = 8
 
  call basis_func(xin,basis)
  call weights(basis,b_inv,w)
  
10 format(A,I1,E25.15)
  ! Print the weights
  print*, "Weights for interpolation"
  do i = 0,n-1
     print*, w(i)
  end do

  ! Test the weights
  do i=0,n-1
     f = f + w(i)*fpoints(i)
  end do

end subroutine 

subroutine nfd1(xin,fpoints,b_inv,f)
  implicit none
  real*8, intent(in) :: xin
  real*8, dimension(0:7), intent(in) :: fpoints
  real*8, dimension(0:7,0:7), intent(in) :: b_inv
  real*8, intent(out) :: f
  real*8, dimension(0:7)  :: x, basis, w
  integer :: i,n

  f = 0
  n = 8

  call basisd1(xin,basis)
  call weights(basis,b_inv,w) 

  print*, "Weights for 1st order differentiation"
  do i = 0,n-1
     print*, w(i)
  end do

  do i=0,n-1
     f = f + w(i)*fpoints(i)
  end do
 
end subroutine

subroutine nfd2(xin,fpoints,b_inv,f)
  implicit none
  real*8, intent(in) :: xin
  real*8, dimension(0:7), intent(in) :: fpoints
  real*8, dimension(0:7,0:7), intent(in) :: b_inv
  real*8, intent(out) :: f
  real*8, dimension(0:7)  :: x, basis, w
  integer :: i,n
 
  f = 0
  n = 8

  call basisd2(xin,basis)
  call weights(basis,b_inv,w)

  print*, "Weights for 2nd order differentiation"
  do i = 0,n-1
     print*, w(i)
  end do

  do i=0,n-1
     f = f + w(i)*fpoints(i)
  end do

end subroutine

subroutine nfint(xin,fpoints,b_inv,f)
  implicit none
  real*8, intent(in) :: xin
  real*8, dimension(0:7), intent(in) :: fpoints
  real*8, dimension(0:7,0:7), intent(in) :: b_inv
  real*8, intent(out) :: f
  real*8, dimension(0:7)  :: x, basis, w
  integer :: i,n

  f = 0
  n = 8

  call basisint(xin,basis)
  call weights(basis,b_inv,w)

  print*, "Weights for integration"
  do i = 0,n-1
     print*, w(i)
  end do

  do i=0,n-1
     f = f + w(i)*fpoints(i)
  end do

end subroutine
