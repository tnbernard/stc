! This is the main file where I call all the subroutines

program main

  implicit none
  real*8 :: x,f
  real*8, dimension(0:7) :: xpoints, fpoints, basis
  real*8, dimension(0:7,0:7)  :: b_matrix, b_inv
  integer :: i,j, n
  
  n = 8
  x = 0.5

1 format(I2,E25.15,E25.15)
  write(*,"(A)") " i             x                       f(x)"
  do i = 0,n-1
     xpoints(i) = 0.0 + 1.0*real(i)/7.0
     fpoints(i) = xpoints(i)**7
     write(*,1) i, xpoints(i), fpoints(i)
  end do
  write(*,*)
  ! Print B and B^-1 matrices
2 format(15E20.10)
  do i = 0,n-1
     call basis_func(xpoints(i),basis)
     do j = 0,n-1
        b_matrix(j,i) = basis(j)
     end do
  end do

  print*, "B = "
  do, i=0,n-1
     write(*,2) ( b_matrix(i,j), j=0,n-1 )
  end do

  call b_inv_func(b_matrix, b_inv)
  print*, "B^(-1) = "
  do, i=0,n-1
    write(*,2) ( b_inv(i,j), j=0, n-1)
  end do
  write(*,*)

10 format(F6.3,E20.10,E20.10,E20.10)

  call nf0(x,fpoints,b_inv,f)
  write(*,*) "Check the interpolation accuracy"
  write(*,"(A)")  " x        numerical value     analytical value    absolute error=|numerical-analytical|"
  write(*,10) 0.5, f, 0.5**7, abs(f - 0.5**7)
  write(*,*)

  call nfd1(x,fpoints,b_inv,f)
  write(*,*) "Check the 1st order differentiation accuracy" 
  write(*,"(A)")  " x        numerical value     analytical value    absolute error=|numerical-analytical|"
  write(*,10) 0.5, f, 7*0.5**6, abs(f - 7*0.5**6)
  write(*,*)

  call nfd2(x,fpoints,b_inv,f)
  write(*,*) "Check the 2nd order differentiation accuracy"
  write(*,"(A)")  " x        numerical value     analytical value    absolute error=|numerical-analytical|"
  write(*,10) 0.5, f, 42*0.5**5, abs(f - 42*0.5**5)
  write(*,*)

  call nfint(x,fpoints,b_inv,f)
  write(*,*) "Check the integration accuracy"
  write(*,"(A)")  " x        numerical value     analytical value    absolute error=|numerical-analytical|"
  write(*,10) 0.5, f, 1.0/8.0*0.5**8, abs(f - 1.0/8.0*0.5**8)

end program
