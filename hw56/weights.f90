!  This file contains all routines related to weights.                         
 
subroutine weights(bin,b_inv,w)     
  implicit none
  real*8, dimension(0:7), intent(in) :: bin
  real*8, dimension (0:7), intent(out) :: w
  real*8, dimension(0:7)  :: x, basis, basis1
  real*8, dimension(0:7,0:7)  :: b_matrix, b_inv, c
  integer :: i,j,n
  real*8  :: s, alpha, beta, x1, f
  
  n = 8
  alpha = 1e0
  beta = 0e0

  do i = 0, n-1
     s = 0
     do j = 0, n-1
        s = s + b_inv(i,j)*bin(j)
     end do
     w(i) = s
  end do
  
end subroutine
