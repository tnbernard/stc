!  This file contains all routines related to the monomial basis.

! Normal basis
subroutine basis_func(x, b)
  implicit none
  real*8, intent(in) :: x
  real*8, dimension(0:7), intent(out) :: b
  integer :: i

  do i = 0,7
     b(i) = x**i
  end do
end subroutine

! First derivative basis
subroutine basisd1(x, b)
  implicit none
  real*8, intent(in) :: x
  real*8, dimension(0:7), intent(out) :: b
  integer :: i

  do i = 0,7
     b(i) = i*x**(i-1)
  end do
end subroutine

! Second derivative basis
subroutine basisd2(x, b)
  implicit none
  real*8, intent(in) :: x
  real*8, dimension(0:7), intent(out) :: b
  integer :: i

  do i = 0,7
     b(i) = i*(i-1)*x**(i-2)
  end do
end subroutine

subroutine basisint(x,b)
  implicit none
  real*8, intent(in) :: x
  real*8, dimension(0:7), intent(out) :: b
  integer :: i

  do i = 0,7
     b(i) = 1.0/(real(i)+1.0)*x**(real(i)+1.0)
  end do
end subroutine

! This subroutine uses LAPACK function to calculate B^(-1)
subroutine b_inv_func(b, b_inv)
  implicit none
  real*8, dimension(0:7)  :: x
  real*8, dimension(0:7,0:7), intent(in)  :: b
  real*8, dimension(0:7,0:7), intent(out) :: b_inv
  real*8, dimension(0:7,0:7) :: b1, c, btest
  real(kind=8) :: alpha, beta
  integer :: i, j, info, lda, ldb, nrhs, n
  integer, dimension(7) :: ipiv

  n = 8
  nrhs = n
  lda = n
  ldb = n

  alpha = 1e0
  beta = 0e0
 
  do j = 0,n-1
     do i = 0,n-1
        if (i == j) then
           b_inv(i,j) = 1
        else
           b_inv(i,j) = 0
        end if
     end do
  end do

10 format(15E20.10)
 
  b1 = b

  call dgesv(n, nrhs, b1, lda, ipiv, b_inv, ldb, info)

  ! Check that the inverse is correct
  !call dgemm('n', 'n', n, n, n, alpha, b_inv, n, b, n, beta, c, n)

  ! Check that product is the identity matrix
  ! print*, "from subroutine, B^(-1)*B = "
  ! do, i=0,n-1
  !   write(*,10) ( c(i,j), j=0, n-1)
  ! end do

  ! call dgemm('n', 'n', n, n, n, alpha, b, n, b_inv, n, beta, c, n)

  ! print*, "B*B^(-1) = "
  ! do, i=0,n-1
  !   write(*,10) ( c(i,j), j=0, n-1)
  ! end do

end subroutine
